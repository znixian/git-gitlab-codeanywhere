# The Basics of Git - Committing, Pulling, Pushing
In git, your repo is made from a large collection of commits. A commit is a snapshot of your code at any given point, along with the unique identifier of whatever commit it was based off, the email of the author, the name of the author, etc. When you have commits, you can imagine them as a graph, like this:
```
.                                                       HEAD
.  *---------------*------------------------------------*
.  C1              C2                                   C3
.  Commit 1        Add some more stuff                  Add some feature
.  znix@znix.xyz   user@example.com                     user2@example.net
.  Campbell Suter  Some Person                          Some Other Person
```

Every commit has a unique identifier, which is a 40 character long string of letters or numbers. While is does uniquely identify a commit, it is also rather awkward to use. Because of this, you can also just use the first 8 characters of the commit ID. Also, you *usually* don't need to use the commit IDs at all, as there are some convenient abbreviations you can use. For example, HEAD refers to the latest commit.

# Adding files
When using git, you first edit your program as usual. When you have made a change you want to save, you need to make a commit. Each commit saves only the changes you made since the last commit, so that your repositories stay small. By default, git assumes that we do not want to store any changes in a commit, so we have to tell it what files we are interested in. Typing `git add -A` tells it that, next time we commit, we want to save the state of the program as it is now. Alternatively, you can type `git add <filename>`, where <filename> is the name of a file you are interested in saving the changes for.

# Making our first commit
Assuming you followed [Chapter 2](../chap2.md), you should have a folder in your CodeAnywhere instance, with the same name as your GitLab project. If not, check that you followed Chapter 2 on this instance, with particular attention to the last section (Cloning).

<div style="float: right;"><image src="../../images/terminal.png" /></div>

Now, as git is a terminal-based program, right-click on your project in CodeAnywhere and select 'SSH Terminal'.

This will open up a terminal running bash, the GNU operating system's command line. In here, you can type a command and then press enter to run it.

First, let's find the current state of our repository:
```
git status
```
This will tell us what is happening. So far, there are no files that it does not know about.

Now, make a file called `README.md`. Put a little bit of text about your project into this file, as it will be displayed on the front page of your GitLab project. Now we have some changes to save, so enter `git status`, and we will see that we have 1 untracked file: README.md.

A file being untracked means that git is currently ignoring it, as it is not considered part of the repository. To add it to the repository, type `git add -A` (add all files, currently just README.md) or type `git add README.md`. Now it is considered part of the repository, and will be committed.

As we want to make our first commit, enter:
```
git commit -m "My First Commit"
```
Let's break this down. In the GNU operating system, there are a lot of commands. As git is a single program, the first part, `git`, tells BASH that we want to run the program named `git`. The next part, `commit`, is a instruction to git to tell it that we want to make a commit. The third part, `-m`, is called a command line option. the `-` says it is a option, and the `m` says we want to set the message of this commit to whatever comes directly after it. The fourth part is, enclosed by quotes, the message of the commit.

Press return, and git will create the new commit. If we want to view all of our commits, type
```
git log
```

This will show a list of all commits, the newest being at the top, and the oldest being at the bottom. If you previously set a license, you will see one it, below your new one. If there are so many it goes off the screen, use the up and down keys to navigate, and press 'q' to exit.

# Pushing to GitLab
We have just made our first commit. It is sitting on CodeAnywhere, waiting for us to do something with it. We would like to have it on GitLab, where we can see it, and download it. To get our code there, you need to *push* your changes over to GitLab. As we cloned the repository from GitLab, it knows where to push it to. We just need to tell it to.
```
git push
```

This will take all the commits that GitLab does not have, and upload them. Once this is done, if you go back to your GitLab project and click on 'commits', you should be able to see your new commit sitting there, having been uploaded.

# Pulling from GitLab
If you, or someone else, has put their new commits onto GitLab, you can use `git pull` to download them to your CodeAnywhere container. Simply type
```
git pull
```
And any new commits will be downloaded and applied.
