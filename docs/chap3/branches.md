# Branches
When programming, you will probably want to be able to work on your project and be able to commit, but without getting in everyone else's way and forcing them to constantly [rebase](chap3/rebase.md) your changes. *Branches* are ideal for this, as they allow you to work on the side of a larger project, without interfering with everyone else.

A branch is a seperate set of commit history and changes, off to the side of the rest of the repository. Here, you can work on your changes until they are ready to be released, at which point you can rebase your changes into the main version, or *master branch*. The master branch (master for short) is a special branch - it is the default branch that you get when you first clone a repository, and it is usually kept in such a state that master will always build and run without issues. All your development can be done on a seperate branch, and then rebased into master.

```
	.  Branches for 'Hello World Program'
	.
	.
	.  Initial Commit                               Add License (GPL3)
	.
	.  *--------------------------------------------*----------------   master
	.   \                                            \          /
	.    \                                            \        /
	.     \                                            \      /
	.      *------------------*-------------------------------          dev 
	.
	.      Start using i18n   Add Geekian translation
```

(Example avaliable on [GitLab](https://gitlab.com/znixian/git-example-project/network/master))

So, let's go through this. First you setup your repository, add a file that says 'Hello, World' and commit it. Now, for further development work, we want to switch to a new branch, which we can use to add and test features before putting stuff on master.
```
git checkout -b dev
```
This says to checkout (switch to) a branch called 'dev', and the '-b' tells git to create the branch. Now, if you run `git branch`, you will see the following output:
```
* dev
  master
```
This tells you that there arer two branches - called 'dev' and 'master'. Whatever branch you are currently on has a star next to it, so in this case we can see that we are on the branch 'dev'.

Now that we have a new branch, we can start adding new features to our program. In this case, I added i18n (localization) support to our Hello, World program. After that, I added a translation for a little-known language called Geekian (language code zz) where 'Hello, World!' is spelt 'ᾘἒἶἶσ, WσřἶϿ!'.

So, now we have some commits on the dev branch. Here is what the repository looks like as a graph:
```
       Initial Commit

       *---------------------------------------------------------------  master
        \
         \
          \
           \
            \
             *--------------------*------------------------------------  dev

             Add i18n Support     Add Geekian Translation
```

Now, if at this point I wanted to copy my changes from the dev branch to the master branch, that would be as easy as as a fast-forward merge. This is because, to take the master branch from where it is now to where the dev branch is now, it would just have to copy across the commits, unmodified. To do this, I would type:
```
git checkout master
git merge --ff-only dev
```

Now, someone else I have given Push access to sees there isn't a license, opens an issue, and everyone agrees that this universal hello world program should be licensed under the GNU GPLv3. So, they go onto GitLab and add a license file, so things now look like this:

```
       Initial Commit                Add License

       *-----------------------------*---------------------------------  master
        \
         \
          \
           \
            \
             *--------------------*------------------------------------  dev

             Add i18n Support     Add Geekian Translation
```
