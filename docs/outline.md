# [Chapter 1 - Introduction](chap1.md)
* What is a SCM
* What is Git
* What is GitLab
* What is CodeAnywhere

# [Chapter 2 - Setup](chap2.md)
* Creating a GitLab project
* Getting our project onto CodeAnywhere

# Chapter 3 - Basics of Git
* [Committing, Pushing, Pulling](chap3/committing-pulling-pushing.md)
* [Branches](chap3/branches.md)
* [Splicing Branches](chap3/splicing.md)
* How to Merge
* How to Rebase
* Basic Workflow

# Chapter 4 - Advanced Git
* Picking Cherries From Other Branches
* Security, Accountability and The Importance of Signing Your Work
* Tags
* Subrepositories - Repo in Repo
* Rewriting History

# Chapter 5 - Advanced GitLab
* Wikis
* Issues
* Merge Requests
* Permissions
