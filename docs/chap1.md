# What is a SCM
A SCM, or Source Code Management program, is a piece of software that can keep track of your software at various times through the development process, and help coordinate multiple people working on the same project, ensuring they don't step on each others toes, and fixing things if they do. If you realize that you liked things how they were several days ago, and that you make major mistakes when writing the changes since then, a SCM can let you rewind your source code to when it was working properly.

# What is Git
[Git][0] is easily the most commonly used SCM in the world. In git, your code is organized into different repositories (repo for short). Each repo has it's own history, which tracks what you have done with your code over time, and branches, which allow you to maintain different versions of your software without having to keep track of multiple copies. Git is a distributed system, although is is commonly used with one central server (which we will use for the purpose of simplicity).

# What is GitLab

When using Git, although you certainly don't have to, you probably want somewhere to store your code. In our case, we will be using what is arguably the best git hosting service available - GitLab. Not only can you store as many repositories as you like for free (any of which can be marked as private, and inaccessible for anyone not logged in as you), but GitLab itself is Free (Libre)/Open Source software (FLOSS), under the terms of the MIT software license. This means that, if you are so inclined, you can run your own copy of GitLab, over which you have total control.

One such service that does this for free is [GitLab][1].

## Why not GitHub?

When thinking about Git hosting, many people will immediately think of [GitHub][2]. It has taken over the vast majority of free software projects. While it is certainly a great service, there are a few problems with it that make GitLab more desirable for many purposes:

-   If you want private repositories (only people selected by you can view them) then you have to pay a monthly fee.
-   While GitHub heavily promotes Free (Libre)/Open Source software (FLOSS), GitHub itself is proprietary software. If GitHub increases their fees, there is nothing you can do. If they shut down, there is nothing you can do. GitLab, on the other hand, offers the Community Edition of GitLab under the BSD license - in other words, you can set up your own copy of GitLab.
-   GitLab has a *lot* more features than GitHub, many of them extremely useful.

# What is CodeAnywhere

Git is easiest to use when you have a command line to use it on. Unfortunately, Windows has an extremely basic and hard to use command line, based off of some version of DOS. The GNU operating system (often used with Linux, a kernel), however, has one of, if not the, best command lines ever created, called `bash`. Also, running the Apache webserver and MariaDB/MySQL database servers is required for what we need to do, using the school computers becomes impossible, as they use Windows. In this case, you need someone else to host a server, which you then can get FTP and command line access to. One such service that does this for free


[0]: https://git-scm.com/  "Git Homepage"
[1]: https://gitlab.com/  "GitLab"
[2]: https://github.com/  "GitHub"
