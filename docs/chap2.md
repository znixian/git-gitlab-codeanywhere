# Creating a GitLab project
The first thing to do when making our first project is to create a repository on GitLab. Go to the [GitLab login page](https://gitlab.com/users/sign_in). Here you can either make a new account, sign in if you already have one, or sign in with oAuth (via Google, Twitter, GitHub or BitBucket).

Now you should be at your projects screen. If not, click the menu button on the top left, and select 'Projects'. This displays all the projects you own.

## Creating the project
Now click 'New Project'. This takes you to the new project page.

![Image of Sign up Page](images/new-project.png "The new project page")

Enter a name for your new project. Add a description, and choose what the permissions for your project are. Private means that only those you invite can access your project, internal means they must be logged into GitLab to access it (not particularly useful here, it is designed for when you are hosting your own copy of GitLab), while public means anyone can see it.

![Image of a New Project](images/project-created.png "After creating a project")

At this point, our new project is ready to start working on. However, you may want to license your project under a Free (Libre) / Open Source Software (FLOSS) license, giving other people the legal ability to use your project, and collaborate with you on it. Otherwise, it is illegal for anyone to use your project without your explicit permission.

## Choosing a license

First, please note that you do not have to put your code under a FLOSS license, if you do not want to. If so, skip this section.

The idea of using a FLOSS license is to make it possible for other people to use and improve your software. There are many reasons for doing this, both [philosophical reasons](https://www.gnu.org/philosophy/free-sw.html), and [practical reasons](https://opensource.org/faq#osd).

If you choose to make your project Free software / Open Source software, you need to choose the license you want to use. The vast majority of FLOSS is under one of these three licenses:

* The MIT/BSD-3-Clause License:
	Do anything you want with this software, so long as you include credit. This includes companies using your software, who can use it without any restriction apart from having to include credit. Is is also extremely short, and very simple. Copy of the [MIT License](https://opensource.org/licenses/mit-license.html). 171 words long.
* Apache 2.0 License:
	Same rough meaning as the MIT license, only it also says that if someone gives you a copy of the software, you grant you a patent license for any patents that they own that the software infringes on. Useful to stop someone including patented code, giving it to you, then suing you for patent infringement. Copy of the [Apache 2.0 License](https://opensource.org/licenses/mit-license.html). 1581 words long.
* GNU General Public License version 3 (GPL):
	The GNU GPL is the most commonly used FLOSS license in existence. It, basically, says that if you give someone a copy of the program, you are also obliged to give them a full copy of the source code (if you have not done this already), under the GPL license. Also, if another piece of software is combined with software under the GPL, the whole thing must be distributed under the GPL. It means that, if someone uses it, and they publish a copy of it, they must also publish the source code and give all recipients the ability to use and redistribute it under the GPL. Although a company could use software you make, they have to publish their software under the GPL, allowing you to use their improvements to your software, along with being able to use their software under the GPL. Copy of the [GNU General Public License version 3](https://www.gnu.org/licenses/gpl.txt). 5644 words long.
	It is worth noting that there are also two other types of GPL, designed to serve simpler purposes [see the GNU License page](https://www.gnu.org/licenses/licenses.html):
	* The GNU Affero General Public License (AGPL)
		The same as the GPL, only it also says that if someone runs it on a web server, and you interact with it through a web browser or something similar, they must be able to give you a copy of the software. This is best used for large projects that run on web servers, such as [OwnCloud](https://owncloud.org/).
	* The GNU Lesser General Public License (LGPL)
		A version of the GPL designed for usage in some libraries. If you write a library for other people to use, and put it under the GPL, then if they use it they must license their project under the GPL as well. The LGPL says that, if you use this code as a library, you must redistribute the LGPL'ed part, but not the rest of the project. In some cases, this is fine (if you have made a one-of-a-kind library, that would be very hard to go without), but in other cases it is better to allow this kind of usage.

In a nutshell:
	* The MIT License is a do-anything license
	* The Apache License is a do-anything license, with some legal safety nets for patents.
	* The GNU GPL means you must license all modifications under the GPL, too.

### Adding a license in GitLab
When you have a empty repository, there is a link to add a license.
![License choosing link](images/license-add.png "How to add a license")
Click 'LICENSE', and it will open up a big text editor. On the top bar of the text editor, on the right hand side, there is a button to choose a license template. Click this, and select your license (all three of the licenses listed above are in the popular section). When you are done, click the big green 'Commit Changes button'.

# Getting our project onto CodeAnywhere
Now we have our project on GitLab, the next step is to copy it onto your CodeAnywhere container (for the purpose of this explanation, I will assume you already have a CodeAnywhere container, or (if you have your own physical server) SSH connection to it).

## Setting up a SSH key.
This must be done only once per container, even if you have multiple projects on one container.

Right-click on the name of your container in the left sidebar, and select 'SSH Terminal'. This opens up a command line we can use.

The first thing we need to do is tell GitLab that we own this container, and to give it access to GitLab.

In your terminal, type `cat ~/.ssh/id_rsa.pub`. This prints out the server's virtual fingerprint, uniquely identifying it. Copy this, then switch back into GitLab. Click the menu button in the top left-hand corner, and select 'Profile Settings'. When your settings have loaded, click on 'SSH Keys' at the top. Paste the key you copied out of CodeAnywhere into the big 'Key' box, under 'Add an SSH key'. Notice that this is broken up into two lines, which are then broken up by word wrap. You then need to select the very start of the third line, and press backspace. You should see something looking like this:
![SSH Key](images/ssh-key.png "SSH Key in GitLab")

## Copying
First, git the git location for your project:
On your project page, next to the star and fork buttons, there will be a dropdown box that either says 'HTTPS' or 'SSH'. If it says 'HTTPS', change it to 'SSH'. Next to that there is text box. Copy the contents of that.

Go to the terminal on CodeAnywhere, and type:
`git clone <link you copied>`
And press return. This will download the project you created into a new folder, ready to start working on.
